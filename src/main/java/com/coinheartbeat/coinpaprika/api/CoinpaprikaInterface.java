package com.coinheartbeat.coinpaprika.api;

import java.io.IOException;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.coinheartbeat.coinpaprika.model.CoinpaprikaCurrency;
import com.coinheartbeat.coinpaprika.model.CoinpaprikaGlobalInfo;
import com.coinheartbeat.coinpaprika.model.CoinpaprikaTicker;

@Path("/v1")
@Produces(MediaType.APPLICATION_JSON)
public interface CoinpaprikaInterface {

	@GET
	@Path("global")
	CoinpaprikaGlobalInfo getGlobalInfo() throws IOException;

	@GET
	@Path("coins")
	CoinpaprikaCurrency[] getCoins() throws IOException;

	@GET
	@Path("ticker")
	CoinpaprikaTicker[] getTickers() throws IOException;

	@GET
	@Path("ticker/{coin_id}")
	CoinpaprikaTicker getTicker(@PathParam("coin_id") String coin_id) throws IOException;

}