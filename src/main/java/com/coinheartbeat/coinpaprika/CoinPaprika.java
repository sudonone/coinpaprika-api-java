package com.coinheartbeat.coinpaprika;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import com.coinheartbeat.coinpaprika.api.CoinpaprikaInterface;
import com.coinheartbeat.coinpaprika.model.CoinpaprikaCurrency;
import com.coinheartbeat.coinpaprika.model.CoinpaprikaGlobalInfo;
import com.coinheartbeat.coinpaprika.model.CoinpaprikaTicker;

import si.mazi.rescu.RestProxyFactory;

/**
 * Base API class used to access basic API functionality.
 *
 * @author Igor < sudonone@gmail.com >
 * 
 */
public final class CoinPaprika {

	private static final CoinpaprikaInterface service = RestProxyFactory.createProxy(CoinpaprikaInterface.class,
			"https://api.coinpaprika.com");

	/**
	 * Get ticker information for specific coin
	 * 
	 * @param coinpaprika custom coin id , i see it's concatenation
	 *                    [symbol-currencyName].tolowerCase
	 * 
	 *                    example: btc-bitcoin
	 * @return
	 * 
	 */
	public static CoinpaprikaTicker getTicker(String id) {
		try {
			return service.getTicker(id);
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * Get ticker information for specific coin
	 * 
	 * @param iso symbol string btc,ltc,eth
	 * @return
	 * 
	 */
	public static CoinpaprikaTicker getTickerBySymbol(String symbol) throws IOException {
		CoinpaprikaCurrency coin = null;
		try {
			coin = Arrays.asList(service.getCoins()).stream().filter(x -> "symbol".equalsIgnoreCase(x.getSymbol()))
					.findAny().orElse(null);
		} catch (IOException e1) {
			e1.printStackTrace();
			return null;
		}
		if (coin != null) {
			return getTicker(coin.getId());
		}
		return null;

	}

	/**
	 * Get ticker information for all coins
	 * 
	 * @return
	 * 
	 */
	public static List<CoinpaprikaTicker> getTickers() {
		try {
			return Arrays.asList(service.getTickers());
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * Fetches all coin listings
	 */
	public static List<CoinpaprikaCurrency> getAllCoins() {
		try {
			return Arrays.asList(service.getCoins());
		} catch (IOException e) {
			e.printStackTrace();
			return Collections.emptyList();
		}
	}

	/**
	 * Fetches new coin listings
	 */
	public static List<CoinpaprikaCurrency> getNewCoins() {
		try {
			return Arrays.asList(service.getCoins()).stream().filter(c -> c.getIsNew() == true)
					.collect(Collectors.toList());
		} catch (IOException e) {
			e.printStackTrace();
			return Collections.emptyList();
		}
	}

	/**
	 * Fetches active coin listings
	 */
	public static List<CoinpaprikaCurrency> getActiveCoins() {
		try {
			return Arrays.asList(service.getCoins()).stream().filter(c -> c.getIsActive() == true)
					.collect(Collectors.toList());
		} catch (IOException e) {
			e.printStackTrace();
			return Collections.emptyList();
		}
	}

	/**
	 * Fetches global information
	 */
	public static CoinpaprikaGlobalInfo getGlobalInfo() {
		try {
			return service.getGlobalInfo();
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}

}
