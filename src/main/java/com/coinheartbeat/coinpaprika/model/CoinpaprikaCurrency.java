package com.coinheartbeat.coinpaprika.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "id", "name", "symbol", "rank", "is_new", "is_active" })
public class CoinpaprikaCurrency {

	@JsonProperty("id")
	private String id;
	@JsonProperty("name")
	private String name;
	@JsonProperty("symbol")
	private String symbol;
	@JsonProperty("rank")
	private Integer rank;
	@JsonProperty("is_new")
	private Boolean isNew;
	@JsonProperty("is_active")
	private Boolean isActive;

	@JsonProperty("id")
	public String getId() {
		return id;
	}

	@JsonProperty("id")
	public void setId(String id) {
		this.id = id;
	}

	@JsonProperty("name")
	public String getName() {
		return name;
	}

	@JsonProperty("name")
	public void setName(String name) {
		this.name = name;
	}

	@JsonProperty("symbol")
	public String getSymbol() {
		return symbol;
	}

	@JsonProperty("symbol")
	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	@JsonProperty("rank")
	public Integer getRank() {
		return rank;
	}

	@JsonProperty("rank")
	public void setRank(Integer rank) {
		this.rank = rank;
	}

	@JsonProperty("is_new")
	public Boolean getIsNew() {
		return isNew;
	}

	@JsonProperty("is_new")
	public void setIsNew(Boolean isNew) {
		this.isNew = isNew;
	}

	@JsonProperty("is_active")
	public Boolean getIsActive() {
		return isActive;
	}

	@JsonProperty("is_active")
	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	@Override
	public String toString() {
		return "CoinpaprikaCurrency [id=" + id + ", name=" + name + ", symbol=" + symbol + ", rank=" + rank + ", isNew="
				+ isNew + ", isActive=" + isActive + "]";
	}

}
