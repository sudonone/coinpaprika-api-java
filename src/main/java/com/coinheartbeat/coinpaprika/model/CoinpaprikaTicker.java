package com.coinheartbeat.coinpaprika.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "id", "name", "symbol", "rank", "price_usd", "price_btc", "volume_24h_usd", "market_cap_usd",
		"circulating_supply", "total_supply", "max_supply", "percent_change_1h", "percent_change_24h",
		"percent_change_7d", "last_updated" })

/**
 * native java object from json
 * 
 * @author sysop
 *
 */
public class CoinpaprikaTicker {

	@JsonProperty("id")
	private String id;
	@JsonProperty("name")
	private String name;
	@JsonProperty("symbol")
	private String symbol;
	@JsonProperty("rank")
	private String rank;
	@JsonProperty("price_usd")
	private String priceUsd;
	@JsonProperty("price_btc")
	private String priceBtc;
	@JsonProperty("volume_24h_usd")
	private String volume24hUsd;
	@JsonProperty("market_cap_usd")
	private String marketCapUsd;
	@JsonProperty("circulating_supply")
	private String circulatingSupply;
	@JsonProperty("total_supply")
	private String totalSupply;
	@JsonProperty("max_supply")
	private String maxSupply;
	@JsonProperty("percent_change_1h")
	private String percentChange1h;
	@JsonProperty("percent_change_24h")
	private String percentChange24h;
	@JsonProperty("percent_change_7d")
	private String percentChange7d;
	@JsonProperty("last_updated")
	private String lastUpdated;

	@JsonProperty("id")
	public String getId() {
		return id;
	}

	@JsonProperty("id")
	public void setId(String id) {
		this.id = id;
	}

	@JsonProperty("name")
	public String getName() {
		return name;
	}

	@JsonProperty("name")
	public void setName(String name) {
		this.name = name;
	}

	@JsonProperty("symbol")
	public String getSymbol() {
		return symbol;
	}

	@JsonProperty("symbol")
	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	@JsonProperty("rank")
	public String getRank() {
		return rank;
	}

	@JsonProperty("rank")
	public void setRank(String rank) {
		this.rank = rank;
	}

	@JsonProperty("price_usd")
	public String getPriceUsd() {
		return priceUsd;
	}

	@JsonProperty("price_usd")
	public void setPriceUsd(String priceUsd) {
		this.priceUsd = priceUsd;
	}

	@JsonProperty("price_btc")
	public String getPriceBtc() {
		return priceBtc;
	}

	@JsonProperty("price_btc")
	public void setPriceBtc(String priceBtc) {
		this.priceBtc = priceBtc;
	}

	@JsonProperty("volume_24h_usd")
	public String getVolume24hUsd() {
		return volume24hUsd;
	}

	@JsonProperty("volume_24h_usd")
	public void setVolume24hUsd(String volume24hUsd) {
		this.volume24hUsd = volume24hUsd;
	}

	@JsonProperty("market_cap_usd")
	public String getMarketCapUsd() {
		return marketCapUsd;
	}

	@JsonProperty("market_cap_usd")
	public void setMarketCapUsd(String marketCapUsd) {
		this.marketCapUsd = marketCapUsd;
	}

	@JsonProperty("circulating_supply")
	public String getCirculatingSupply() {
		return circulatingSupply;
	}

	@JsonProperty("circulating_supply")
	public void setCirculatingSupply(String circulatingSupply) {
		this.circulatingSupply = circulatingSupply;
	}

	@JsonProperty("total_supply")
	public String getTotalSupply() {
		return totalSupply;
	}

	@JsonProperty("total_supply")
	public void setTotalSupply(String totalSupply) {
		this.totalSupply = totalSupply;
	}

	@JsonProperty("max_supply")
	public String getMaxSupply() {
		return maxSupply;
	}

	@JsonProperty("max_supply")
	public void setMaxSupply(String maxSupply) {
		this.maxSupply = maxSupply;
	}

	@JsonProperty("percent_change_1h")
	public String getPercentChange1h() {
		return percentChange1h;
	}

	@JsonProperty("percent_change_1h")
	public void setPercentChange1h(String percentChange1h) {
		this.percentChange1h = percentChange1h;
	}

	@JsonProperty("percent_change_24h")
	public String getPercentChange24h() {
		return percentChange24h;
	}

	@JsonProperty("percent_change_24h")
	public void setPercentChange24h(String percentChange24h) {
		this.percentChange24h = percentChange24h;
	}

	@JsonProperty("percent_change_7d")
	public String getPercentChange7d() {
		return percentChange7d;
	}

	@JsonProperty("percent_change_7d")
	public void setPercentChange7d(String percentChange7d) {
		this.percentChange7d = percentChange7d;
	}

	@JsonProperty("last_updated")
	public String getLastUpdated() {
		return lastUpdated;
	}

	@JsonProperty("last_updated")
	public void setLastUpdated(String lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

}
