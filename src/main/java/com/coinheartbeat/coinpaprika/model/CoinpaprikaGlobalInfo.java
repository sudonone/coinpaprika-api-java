package com.coinheartbeat.coinpaprika.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "market_cap_usd", "volume_24h_usd", "bitcoin_dominance_percentage", "cryptocurrencies_number",
		"last_updated" })
public class CoinpaprikaGlobalInfo {

	@JsonProperty("market_cap_usd")
	private Integer marketCapUsd;
	@JsonProperty("volume_24h_usd")
	private Integer volume24hUsd;
	@JsonProperty("bitcoin_dominance_percentage")
	private Double bitcoinDominancePercentage;
	@JsonProperty("cryptocurrencies_number")
	private Integer cryptocurrenciesNumber;
	@JsonProperty("last_updated")
	private Integer lastUpdated;

	@JsonProperty("market_cap_usd")
	public Integer getMarketCapUsd() {
		return marketCapUsd;
	}

	@JsonProperty("market_cap_usd")
	public void setMarketCapUsd(Integer marketCapUsd) {
		this.marketCapUsd = marketCapUsd;
	}

	@JsonProperty("volume_24h_usd")
	public Integer getVolume24hUsd() {
		return volume24hUsd;
	}

	@JsonProperty("volume_24h_usd")
	public void setVolume24hUsd(Integer volume24hUsd) {
		this.volume24hUsd = volume24hUsd;
	}

	@JsonProperty("bitcoin_dominance_percentage")
	public Double getBitcoinDominancePercentage() {
		return bitcoinDominancePercentage;
	}

	@JsonProperty("bitcoin_dominance_percentage")
	public void setBitcoinDominancePercentage(Double bitcoinDominancePercentage) {
		this.bitcoinDominancePercentage = bitcoinDominancePercentage;
	}

	@JsonProperty("cryptocurrencies_number")
	public Integer getCryptocurrenciesNumber() {
		return cryptocurrenciesNumber;
	}

	@JsonProperty("cryptocurrencies_number")
	public void setCryptocurrenciesNumber(Integer cryptocurrenciesNumber) {
		this.cryptocurrenciesNumber = cryptocurrenciesNumber;
	}

	@JsonProperty("last_updated")
	public Integer getLastUpdated() {
		return lastUpdated;
	}

	@JsonProperty("last_updated")
	public void setLastUpdated(Integer lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

}
